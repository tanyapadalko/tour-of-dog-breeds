export enum DogGroup {
  Sporting = 'Sporting Group',
  Working = 'Working Group',
  Toy = 'Toy Group',
  Herding = 'Herding Group',
  Hound = 'Hound Group',
  Terrier = 'Terrier Group',
  Non_sporting = 'Non-Sporting Group',
  Miscellaneous = 'Miscellaneous Class',
  Foundation_Stock_Service = 'Foundation Stock Service'
}
