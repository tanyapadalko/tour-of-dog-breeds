import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DogDetailComponent} from './dog-detail/dog-detail.component';
import {DogsComponent} from './dogs/dogs.component';
import {DashboardComponent} from './dashboard/dashboard.component';


const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'detail/:id', component: DogDetailComponent },
  { path: 'dogs', component: DogsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
