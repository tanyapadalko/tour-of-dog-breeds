import {Component, Input, OnInit} from '@angular/core';
import {Dog} from '../dog';
import {DogService} from '../dog.service';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';


@Component({
  selector: 'app-dog-detail',
  templateUrl: './dog-detail.component.html',
  styleUrls: ['./dog-detail.component.css']
})
export class DogDetailComponent implements OnInit {

  @Input() dog: Dog;
  // dogGroups: String[] = ['Sporting Group', 'Working Group', 'Hound Group', 'Herding Group', 'Toy Group'];
  dogGroups: string[];


  constructor(private dogService: DogService, private location: Location, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.getDogGroups();
    this.getDog();
  }

  getDogGroups(): void {
    this.dogService.getDogGroups().subscribe(dogGroups => this.dogGroups = dogGroups);
  }

  getDog(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.dogService.getDog(id)
      .subscribe(dog => this.dog = dog);
  }

  goBack(): void {
    this.location.back();
  }

}
