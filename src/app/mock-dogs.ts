import {Dog} from './dog';
import {DogGroup} from './dogGroup';

export const DOGS: Dog[] = [
  {
    id: 1,
    name: 'Afghan Hound',
    heightFrom: 25,
    heightTo: 27,
    weightFrom: 50,
    weightTo: 60,
    lifeExpectancyFrom: 12,
    lifeExpectancyTo: 18,
    group: DogGroup.Hound
  },
  {
    id: 2,
    name: 'Beagle',
    heightFrom: 13,
    heightTo: 15,
    weightFrom: 20,
    weightTo: 30,
    lifeExpectancyFrom: 10,
    lifeExpectancyTo: 15,
    group: DogGroup.Hound
  },
  {
    id: 3,
    name: 'Petit Basset Griffon Vendéen',
    heightFrom: 13,
    heightTo: 15,
    weightFrom: 25,
    weightTo: 40,
    lifeExpectancyFrom: 14,
    lifeExpectancyTo: 16,
    group: DogGroup.Hound
  },
  {
    id: 4,
    name: 'Borzoi',
    heightFrom: 26,
    heightTo: null,
    weightFrom: 60,
    weightTo: 105,
    lifeExpectancyFrom: 9,
    lifeExpectancyTo: 14,
    group: DogGroup.Hound
  },
  {
    id: 5,
    name: 'Basset Hound',
    heightFrom: null,
    heightTo: 15,
    weightFrom: 40,
    weightTo: 65,
    lifeExpectancyFrom: 12,
    lifeExpectancyTo: 13,
    group: DogGroup.Hound
  },
  {
    id: 6,
    name: 'Saluki',
    heightFrom: 23,
    heightTo: 28,
    weightFrom: 40,
    weightTo: 65,
    lifeExpectancyFrom: 10,
    lifeExpectancyTo: 17,
    group: DogGroup.Hound
  },
  {
    id: 7,
    name: 'Scottish Deerhound',
    heightFrom: 30,
    heightTo: 32,
    weightFrom: 85,
    weightTo: 110,
    lifeExpectancyFrom: 8,
    lifeExpectancyTo: 11,
    group: DogGroup.Hound
  },
  {
    id: 8,
    name: 'American Leopard Hound',
    heightFrom: 21,
    heightTo: 27,
    weightFrom: 45,
    weightTo: 70,
    lifeExpectancyFrom: 12,
    lifeExpectancyTo: 15,
    group: DogGroup.Foundation_Stock_Service
  }
];
