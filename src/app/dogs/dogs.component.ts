import {Component, OnInit} from '@angular/core';
import {Dog} from '../dog';
import {DogService} from '../dog.service';


@Component({
  selector: 'app-dogs',
  templateUrl: './dogs.component.html',
  styleUrls: ['./dogs.component.css']
})
export class DogsComponent implements OnInit {

  dogGroups: string[];
  dogs: Dog[];
  selectedDog: Dog;

  constructor(private dogService: DogService) {
  }

  ngOnInit() {
    this.getDogs();
    this.getDogGroups();
  }

  onSelect(dog: Dog): void {
    this.selectedDog = dog;
  }

  getDogs(): void {
    this.dogService.getDogs().subscribe(dogs => this.dogs = dogs);
  }

  getDogGroups(): void {
    this.dogService.getDogGroups().subscribe(dogGroups => this.dogGroups = dogGroups);
  }

}
