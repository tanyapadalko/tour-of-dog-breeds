import {DogGroup} from './dogGroup';

export class Dog {
  id: number;
  name: string;
  heightFrom: number; // inches
  heightTo: number;
  weightFrom: number; // pounds
  weightTo: number;
  lifeExpectancyFrom: number; // years
  lifeExpectancyTo: number;
  group: DogGroup;
}
