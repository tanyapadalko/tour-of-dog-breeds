import {Injectable} from '@angular/core';
import {Dog} from './dog';
import {DOGS} from './mock-dogs';
import {DogGroup} from './dogGroup';
import {MessageService} from './message.service';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DogService {

  constructor(private messageService: MessageService) {
  }

  getDogs(): Observable<Dog[]> {
    this.messageService.add('DogService: fetched dogs');
    return of(DOGS);
  }


  getDogGroups(): Observable<string[]> {
    this.messageService.add('DogService: fetched dog groups');
    return of(Object.keys(DogGroup));
  }

  getDog(id: number): Observable<Dog> {
    this.messageService.add(`DogService: fetched dog id=${id}`);
    return of(DOGS.find(dog => dog.id === id));
  }

}
